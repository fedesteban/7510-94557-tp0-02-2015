package ar.fiuba.tdd.tp0;

import java.util.LinkedList;
import java.util.NoSuchElementException;

class RPNCalculator{
    private OperatorDB myOperatorDB;
    private Parser myParser;
    private LinkedList<Float> operands;

    RPNCalculator() {
        myOperatorDB = new OperatorDB();
        myParser = new Parser();
        operands = new LinkedList<>();
    }

    public float eval(final String expression) {
        try {
            final String[] parameters = myParser.parse(expression);

            for (final String operand : parameters) {
                try {
                    operands.addLast(Float.parseFloat(operand));
                }
                catch (NumberFormatException e) {
                    final Operator myOperator = myOperatorDB.getOperation(operand);
                    final OperatorDB.IntegerMath myFunction = myOperatorDB.getFunction(operand);
                    final float result = myOperator.operate(operands, myFunction);
                    operands.addFirst(result);
                }
            }

            return operands.remove();
        }
        catch (NoSuchElementException | NullPointerException e0) {
            throw new IllegalArgumentException();
        }
    }
}
