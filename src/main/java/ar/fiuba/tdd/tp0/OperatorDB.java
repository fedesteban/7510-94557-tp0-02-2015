package ar.fiuba.tdd.tp0;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Esteban Federico on 01/09/2015.
 */
class OperatorDB {

    /*
    Podria usar un solo map de tener disponible una clase Pair o Tuple
     */
    private final Map<String, String> operatorMap = new HashMap<>();
    private final Map<String, IntegerMath> functionMap = new HashMap<>();

    final IntegerMath addition = (firstOperand, secondOperand) -> firstOperand + secondOperand;
    final IntegerMath subtraction = (firstOperand, secondOperand) -> firstOperand - secondOperand;
    final IntegerMath multiplication = (firstOperand, secondOperand) -> firstOperand * secondOperand;
    final IntegerMath division = (firstOperand, secondOperand) -> firstOperand / secondOperand;
    final IntegerMath module = (firstOperand, secondOperand) -> firstOperand % secondOperand;

    interface IntegerMath {
        float operation(float firstOperand, float secondOperand);
    }

    public OperatorDB() {

        operatorMap.put("+", "BynaryOperator");
        operatorMap.put("-", "BynaryOperator");
        operatorMap.put("*", "BynaryOperator");
        operatorMap.put("/", "BynaryOperator");
        operatorMap.put("MOD", "BynaryOperator");
        operatorMap.put("++", "NaryOperator");
        operatorMap.put("--", "NaryOperator");
        operatorMap.put("**", "NaryOperator");
        operatorMap.put("//", "NaryOperator");

        functionMap.put("+", addition);
        functionMap.put("-", subtraction);
        functionMap.put("*", multiplication);
        functionMap.put("/", division);
        functionMap.put("MOD", module);
        functionMap.put("++", addition);
        functionMap.put("--", subtraction);
        functionMap.put("**", multiplication);
        functionMap.put("//", division);
    }

    public Operator getOperation(final String operand) {
        final String stringOfOperator = operatorMap.get(operand);
        return Operator.valueOf(stringOfOperator);
    }

    public IntegerMath getFunction(String operand) {
        return functionMap.get(operand);
    }
}
