package ar.fiuba.tdd.tp0;

import java.util.LinkedList;

/**
 * Created by Esteban Federico on 01/09/2015.
 */
public enum Operator {

    BynaryOperator() {
        @Override public float operate(final LinkedList<Float> operands, OperatorDB.IntegerMath op) {
            float secondOperand = operands.removeLast();
            float firstOperand = operands.removeLast();
            return op.operation(firstOperand, secondOperand);
        }
    },

    NaryOperator() {
        @Override public float operate(final LinkedList<Float> operands, OperatorDB.IntegerMath op) {
            while (operands.size() > 1) {
                float firstOperand = operands.removeFirst();
                float secondOperand = operands.removeFirst();

                operands.addFirst(op.operation(firstOperand, secondOperand));
            }
            return operands.removeLast();
        }
    };

    public abstract float operate(final LinkedList<Float> operands, OperatorDB.IntegerMath op);
}
