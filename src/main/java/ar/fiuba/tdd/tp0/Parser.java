package ar.fiuba.tdd.tp0;

/**
 * Created by Esteban Federico on 01/09/2015.
 */
class Parser {
    public Parser(){}
    public String[] parse(final String expression) {
        return expression.split(" ");
    }
}
